using System;
using Random = UnityEngine.Random;

public class MinMaxRangeAttribute : Attribute
{
    public MinMaxRangeAttribute(float min, float max)
    {
        Min = min;
        Max = max;
    }

    public float Min { get; private set; }
    public float Max { get; private set; }
}


/// <summary>
/// A float pair with a custom min and a max value. Updated 1.1
/// *requires Attribute [MinMaxRangeAttribute(a,b)] to set/see in inspector;
/// </summary>
[Serializable]
public struct RangedFloat
{
    public float minValue;
    public float maxValue;
   
    public float random
    {
        get
        {
            RangedFloat rf = this;
            return Random.Range(rf.minValue, rf.maxValue);
        }
    }
}

